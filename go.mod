module bitbucket.org/canonical-ledgers/factom-testload

go 1.12

require (
	github.com/AdamSLevy/factom v0.0.0-20181009222153-a7ad13c1f605
	github.com/hashicorp/errwrap v0.0.0-20180715044906-d6c0cd880357 // indirect
	github.com/hashicorp/go-multierror v0.0.0-20180717150148-3d5d8f294aa0 // indirect
	github.com/posener/complete v1.1.1
	github.com/satori/go.uuid v1.2.0
	github.com/sirupsen/logrus v1.0.6
	golang.org/x/crypto v0.0.0-20180806190021-80fca2ff14a3 // indirect
	golang.org/x/sys v0.0.0-20180806192500-2be389f392cd // indirect
	gopkg.in/airbrake/gobrake.v2 v2.0.9 // indirect
	gopkg.in/gemnasium/logrus-airbrake-hook.v2 v2.1.2 // indirect
)
