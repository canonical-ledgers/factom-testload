package main

import (
	"fmt"
	"math/rand"
	"time"

	"github.com/AdamSLevy/factom"
	uuid "github.com/satori/go.uuid"
	log "github.com/sirupsen/logrus"
)

func generateLoad() {
	waitTime := 0 * time.Second
	i := 0
	for {
		select {
		case <-time.After(waitTime):
			// Randomly make a new chain or entry on an existing
			// chain. Always make a new chain if we haven't created
			// any yet.
			if numChains == 0 || rand.Intn(100) < percentNewChain {
				chain := newChain(ecAddress)
				// Loop through our saved chains. Replace the
				// oldest chain if we have already made
				// maxChains.
				chains[i] = chain
				i++
				if i == maxChains {
					i = 0
				}
				// Count the number of chains we have saved up
				// until we have maxChains.
				if numChains < maxChains {
					numChains++
				}
				log.Printf("New Chain: %v", chain.ChainID)
			} else {
				entry := newEntry(chains[rand.Intn(numChains)], ecAddress)
				log.Printf("New Entry: %x", entry.Hash())
			}
		}
		// Set up waitTime.
		jitter := time.Duration(
			float64(entryJitter) * float64(rand.Intn(100)) / 100.0)
		if rand.Intn(2) == 1 {
			waitTime = entryInterval + jitter
		} else {
			waitTime = entryInterval - jitter
		}
		//log.Println("Waiting", waitTime, "before making the next entry.")
	}
}

func newExtIDs() [][]byte {
	return [][]byte{[]byte("Factom Testload Daemon"),
		[]byte(uuid.NewV4().String())}
}

func newContent() []byte {
	content := make([]byte, entrySize)
	_, err := rand.Read(content)
	if err != nil {
		log.Fatal(err)
	}
	return content
}

func newChain(ecAddress *factom.ECAddress) *factom.Chain {
	entry := &factom.Entry{}
	entry.Content = newContent()
	entry.ExtIDs = newExtIDs()
	chain := factom.NewChain(entry)
	// check ec address balance
	balance, err := factom.GetECBalance(ecAddress.PubString())
	if err != nil {
		log.Fatal(err)
	}
	if cost, err := factom.EntryCost(chain.FirstEntry); err != nil {
		log.Fatal(err)
	} else if balance < int64(cost)+10 {
		log.Fatal("Not enough Entry Credits")
	}
	txid, err := factom.CommitChain(chain, ecAddress)
	if err != nil {
		log.Fatal("Commit chain ", err)
	}
	_, err = waitOnCommitAck(txid)
	if err != nil {
		log.Fatal("Commit chain ack ", err)
	}
	txid, err = factom.RevealChain(chain)
	if err != nil {
		log.Fatal("Reveal chain ", err)
	}
	_, err = waitOnRevealAck(txid)
	if err != nil {
		log.Fatal("Reveal chain ack ", err)
	}
	return chain
}

func newEntry(chain *factom.Chain, ecAddress *factom.ECAddress) *factom.Entry {
	entry := &factom.Entry{}
	entry.Content = newContent()
	entry.ChainID = chain.ChainID
	entry.ExtIDs = newExtIDs()
	// check ec address balance
	balance, err := factom.GetECBalance(ecAddress.PubString())
	if err != nil {
		log.Fatal(err)
	}
	if cost, err := factom.EntryCost(entry); err != nil {
		log.Fatal(err)
	} else if balance < int64(cost)+10 {
		log.Fatal("Not enough Entry Credits")
	}
	txid, err := factom.CommitEntry(entry, ecAddress)
	if err != nil {
		log.Fatal("Commit entry ", err)
	}
	_, err = waitOnCommitAck(txid)
	if err != nil {
		log.Fatal("Commit entry ack ", err)
	}
	txid, err = factom.RevealEntry(entry)
	if err != nil {
		log.Fatal("Reveal entry ", err)
	}
	_, err = waitOnRevealAck(txid)
	if err != nil {
		log.Fatal("Reveal entry ack ", err)
	}
	return entry
}

func waitOnCommitAck(txid string) (string, error) {
	stat := make(chan string, 1)
	errchan := make(chan error, 1)

	// poll for the acknowledgement
	go func() {
		for {
			s, err := factom.EntryCommitACK(txid, "")
			if err != nil {
				errchan <- err
				break
			}

			if (s.CommitData.Status != "Unknown") && (s.CommitData.Status != "NotConfirmed") {
				stat <- s.CommitData.Status
				break
			}
			time.Sleep(time.Second / 2)
		}
	}()

	// wait for the acknowledgement or timeout after 10 sec
	select {
	case err := <-errchan:
		return "", err
	case s := <-stat:
		return s, nil
	case <-time.After(timeout):
		return "", fmt.Errorf("timeout: no acknowledgement found")
	}

	// code should not reach this point
	return "", fmt.Errorf("unknown error")
}

// waitOnRevealAck blocks while waiting for an ack message for an Entry Reveal
// and returns the ack status or times out after 10 seconds.
func waitOnRevealAck(txid string) (string, error) {
	stat := make(chan string, 1)
	errchan := make(chan error, 1)

	// poll for the acknowledgement
	go func() {
		for {
			// All 0s signals an entry
			s, err := factom.EntryRevealACK(txid, "", "0000000000000000000000000000000000000000000000000000000000000000")
			if err != nil {
				errchan <- err
				break
			}

			if (s.EntryData.Status != "Unknown") && (s.EntryData.Status != "NotConfirmed") {
				stat <- s.EntryData.Status
				break
			}
			time.Sleep(time.Second / 2)
		}
	}()

	// wait for the acknowledgement or timeout after 10 sec
	select {
	case err := <-errchan:
		return "", err
	case s := <-stat:
		return s, nil
	case <-time.After(timeout):
		return "", fmt.Errorf("timeout: no acknowledgement found")
	}

	// code should not reach this point
	return "", fmt.Errorf("unknown error")
}
