package main

import (
	"flag"
	"os"
	"strings"
	"time"

	"github.com/AdamSLevy/factom"
	"github.com/posener/complete"
)

var (
	completion *complete.Complete

	pubECAddress, factomdServersCSV                   string
	percentNewChain, entrySize, maxChains, numThreads int
	entryInterval, entryJitter, timeout               time.Duration
)

func init() {
	flag.StringVar(&pubECAddress, "ecaddress", "",
		"Public EC address to use to pay for entries [required]")
	flag.IntVar(&percentNewChain, "percentnewchain", 20,
		"Percentage of entries that will create a new chain")
	flag.IntVar(&entrySize, "entrysize", 1024,
		"Size of entry content in bytes")
	flag.IntVar(&maxChains, "maxchains", 10,
		"Max number of chains to store in memory at one time")
	//flag.IntVar(&numThreads, "threads", 2,
	//	"Number of threads to launch to make requests in parallel")
	flag.DurationVar(&entryInterval, "interval", 1*time.Minute,
		"Interval between entries")
	flag.DurationVar(&entryJitter, "jitter", 10*time.Second,
		"Add a random jitter to the interval between entries. "+
			"Entries will occur every interval +/- jitter.")
	flag.DurationVar(&timeout, "timeout", 10*time.Second,
		"Timeout for calls to factomd or factom-walletd")

	rpc := factom.RpcConfig

	flag.StringVar(&factomdServersCSV, "s", "localhost:8088",
		"One or more IPAddr:port# in a comma separated list of factomd API endpoints to use to access blockchain")
	flag.StringVar(&rpc.FactomdRPCUser, "factomduser", "",
		"Username for API connections to factomd")
	flag.StringVar(&rpc.FactomdRPCPassword, "factomdpassword", "",
		"Password for API connections to factomd")
	flag.StringVar(&rpc.FactomdTLSCertFile, "factomdcert",
		"~/.factom/m2/factomdAPIpub.cert",
		"This file is the TLS certificate provided by the factomd API")
	flag.BoolVar(&rpc.FactomdTLSEnable, "factomdtls", false,
		"Set to true when the factomd API is encrypted")

	flag.StringVar(&rpc.WalletServer, "w", "localhost:8089",
		"IPAddr:port# of factom-walletd API to use to create transactions")
	flag.StringVar(&rpc.WalletRPCUser, "walletuser", "",
		"Username for API connections to factom-walletd")
	flag.StringVar(&rpc.WalletRPCPassword, "walletpassword", "",
		"Password for API connections to factom-walletd")
	flag.StringVar(&rpc.WalletTLSCertFile, "walletcert",
		"~/.factom/m2/factom-walletdAPIpub.cert",
		"This file is the TLS certificate provided by the factom-walletd API")
	flag.BoolVar(&rpc.WalletTLSEnable, "wallettls", false,
		"Set to true when the factom-walletd API is encrypted")

	cmd := complete.Command{
		Flags: complete.Flags{
			"-ecaddress":       predictECAddress,
			"-percentnewchain": complete.PredictAnything,
			"-interval":        complete.PredictAnything,
			"-timeout":         complete.PredictAnything,
			"-maxchains":       complete.PredictAnything,
			"-jitter":          complete.PredictAnything,
			"-entrysize":       complete.PredictAnything,

			"-factomdcert":     complete.PredictFiles("*"),
			"-factomdpassword": complete.PredictAnything,
			"-factomdtls":      complete.PredictNothing,
			"-factomduser":     complete.PredictAnything,
			"-s":               complete.PredictAnything,
			"-w":               complete.PredictAnything,
			"-walletcert":      complete.PredictFiles("*"),
			"-walletpassword":  complete.PredictAnything,
			"-wallettls":       complete.PredictNothing,
			"-walletuser":      complete.PredictAnything,

			"-y":                   complete.PredictNothing,
			"-installcompletion":   complete.PredictNothing,
			"-uninstallcompletion": complete.PredictNothing,
		},
	}
	completion = complete.New(os.Args[0], cmd)
	completion.CLI.InstallName = "installcompletion"
	completion.CLI.UninstallName = "uninstallcompletion"
	completion.AddFlags(nil)
}

var flags *flag.FlagSet

// Parse any previously specified factom-walletd options required for
// connecting to factom-walletd
func parseWalletFlags() {
	if flags != nil {
		// We already parsed the flags.
		return
	}
	// Using flag.FlagSet allows us to parse a custom array of flags
	// instead of this programs args.
	flags = flag.NewFlagSet("", flag.ContinueOnError)
	flags.StringVar(&factom.RpcConfig.WalletServer, "w", "localhost:8089", "")
	flags.StringVar(&factom.RpcConfig.WalletTLSCertFile, "walletcert",
		"~/.factom/walletAPIpub.cert", "")
	flags.StringVar(&factom.RpcConfig.WalletRPCUser, "walletuser", "", "")
	flags.StringVar(&factom.RpcConfig.WalletRPCPassword, "walletpassword", "", "")
	flags.BoolVar(&factom.RpcConfig.WalletTLSEnable, "wallettls", false, "")

	// flags.Parse will print warnings if it comes across an unrecognized
	// flag. We don't want this so we temprorarily redirect everything to
	// /dev/null before we call flags.Parse().
	stdout := os.Stdout
	stderr := os.Stderr
	os.Stdout, _ = os.Open(os.DevNull)
	os.Stderr = os.Stdout

	// The current command line being typed is stored in the environment
	// variable COMP_LINE. We split on spaces and discard the first in the
	// list because it is the program name `factom-cli`.
	flags.Parse(strings.Fields(os.Getenv("COMP_LINE"))[1:])

	// Restore stdout and stderr.
	os.Stdout = stdout
	os.Stderr = stderr

	// We want need factom-walletd to timeout or the CLI completion will
	// hang and never return. This is the whole reason we use AdamSLevy's
	// fork of factom.
	factom.SetWalletTimeout(1 * time.Second)
}

var predictECAddress = complete.PredictFunc(func(a complete.Args) []string {
	_, ecs := addressPubStrings()
	return ecs
})

func addressPubStrings() ([]string, []string) {
	parseWalletFlags()
	// Fetch all addresses.
	fcts, ecs, err := factom.FetchAddresses()
	if err != nil {
		complete.Log("error: %v", err)
		return nil, nil
	}

	// Create slices of the public address strings.
	fctAddresses := make([]string, len(fcts))
	for i, fct := range fcts {
		fctAddresses[i] = fct.String()
	}
	ecAddresses := make([]string, len(ecs))
	for i, ec := range ecs {
		ecAddresses[i] = ec.PubString()
	}
	return fctAddresses, ecAddresses
}
