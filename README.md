# Factom Testload
This program generates random chains and entries on the Factom blockchain for
use on the testnet.

## DISCLAIMER
THIS TOOL CAN BE USED TO CREATE A LOT OF LOAD ON THE FACTOM NETWORK. IT IS FOR
TESTING PURPOSES ONLY AND SHOULD ONLY BE USED ON TESTNET. YOU WILL BURN THROUGH
A LOT OF ENTRY CREDITS SO MAKE SURE YOU DON'T SPEND YOUR REAL MAINNET ENTRY
CREDITS!

## Installation and Setup
Build and install the program:
```
go get -u bitbucket.org/canonical-ledgers/factom-testload
go install bitbucket.org/canonical-ledgers/factom-testload
```
Install command line completion:
```
factom-testload -installcompletion -y
source ~/.bashrc
```

### Factomd
You must have access to the RPC API endpoint of a synced
[`factomd`](https://github.com/FactomProject/factomd) node. You must supply the
host and port number to `factom-testload` using the `-s` option if it is not at
the default location (`localhost:8088`).

### Factom-walletd
You must have access to the RPC API endpoint of a synced
[`factom-walletd`](https://github.com/FactomProject/factom-walletd) node. You
must supply the host and port number to `factom-testload` using the `-w` option
if it is not at the default location (`localhost:8089`).

## Basic usage
Assuming that `factomd` and `factom-walletd` are available on `localhost` on
their standard ports, `8088` and `8089`, respectively, then you can start the
testload generator with the default settings:
```
factom-testload -ecaddress EC3emTZegtoGuPz3MRA4uC8SU6Up52abqQUEKqW44TppGGAc4Vrq
```

## Options
### EC Config
You must supply a valid EC public address that is stored with `factom-walletd`.
```
  -ecaddress string
    	Public EC address to use to pay for entries
```

### Load Generation Options
Additional options can configure how much load will be generated. These
settings will affect the rate at which we consume ECs.
```
  -entrysize int
    	Size of entry content in bytes (default 1024)
  -interval duration
    	Interval between entries (default 1m0s)
  -jitter duration
    	Add a random jitter to the interval between entries. Entries will occur every interval +/- jitter. (default 10s)
  -percentnewchain int
    	Percentage of entries that will create a new chain (default 20)
```

### Factomd/factom-walletd Options
These options set how we connect to `factomd` and `factom-walletd`.
```
  -factomdcert string
    	This file is the TLS certificate provided by the factomd API (default "~/.factom/m2/factomdAPIpub.cert")
  -factomdpassword string
    	Password for API connections to factomd
  -factomdtls
    	Set to true when the factomd API is encrypted
  -factomduser string
    	Username for API connections to factomd
  -s string
    	IPAddr:port# of factomd API to use to access blockchain (default "localhost:8088")
  -w string
    	IPAddr:port# of factom-walletd API to use to create transactions (default "localhost:8089")
  -walletcert string
    	This file is the TLS certificate provided by the factom-walletd API (default "~/.factom/m2/factom-walletdAPIpub.cert")
  -walletpassword string
    	Password for API connections to factom-walletd
  -wallettls
    	Set to true when the factom-walletd API is encrypted
  -walletuser string
    	Username for API connections to factom-walletd
```

### Completion Options
```
  -installcompletion
    	Install completion for factom-testload command
  -uninstallcompletion
    	Uninstall completion for factom-testload command
  -y	Don't prompt user for typing 'yes'
```

## TODO
- Better cli option error checking.
- Autoscale jitter to 10% of interval size.
