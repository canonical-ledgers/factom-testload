package main

import (
	"flag"
	"math/rand"
	"strings"
	"time"

	"github.com/AdamSLevy/factom"
	//_ "github.com/mattn/go-sqlite3"
	log "github.com/sirupsen/logrus"
)

var (
	numChains      = 0
	chains         []*factom.Chain
	ecAddress      *factom.ECAddress
	factomdServers []string
)

func main() {
	// Attempt to run the completion program.
	if completion.Complete() {
		// The completion program ran, so just return.
		return
	}

	// Parse CLI flags. See flag.go for flag spec.
	flag.Parse()

	log.Println("Generating random chains and entries...")

	chains = make([]*factom.Chain, maxChains)

	// Set up a random seed based on the time.
	rand.Seed(time.Now().UTC().UnixNano())

	// Attempt to retrieve the specified EC address.
	var err error
	ecAddress, err = factom.FetchECAddress(pubECAddress)
	if err != nil {
		log.Fatal(err)
	}

	// Parse comma separated list of factomd endpoints
	factomdServers = strings.Split(factomdServersCSV, ",")
	factom.RpcConfig.FactomdServer = factomdServers[0]

	generateLoad()
}
